package config

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	DBUser     = "root"
	DBPassword = ""
	DBName     = "ppdb"
	DBHost     = "localhost"
	DBPort     = "3306"
	DBType     = "mysql"
)

func getDBType() string {
	return DBType
}

func getSQLConnection() string {
	database := fmt.Sprintf("%v:%v@(%v:%v)/%v?parseTime=True",
		DBUser,
		DBPassword,
		DBHost,
		DBPort,
		DBName)
	return database
}

func InitDB() *gorm.DB {
	var err error
	initDB, err := gorm.Open(mysql.Open(getSQLConnection()), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		panic("Failed Connection Database")
	}

	return initDB
}

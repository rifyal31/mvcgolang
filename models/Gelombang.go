package models

type Gelombang struct {
	Id            int    `json: id`
	NamaGelombang string `json: nama_gelombang`
	TahunAjaran   string `json: tahun_ajaran`
	TglBuka       string `json: tgl_buka`
	TglTutup      string `json: tgl_tutup`
	Kuota         string `json: kuota`
	Status        string `json: status`
}

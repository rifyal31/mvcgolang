package models

type Pendaftar struct {
	Id   int `json:id_pendaftar`
	Nisn int `json:nisn`
	// Password   string `json:password`
	IbuKandung string `json:ibu_kandung`
	Status     int    `json:status`
}

package routers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Web() {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "HALAMAN UTAMA")
	})

	Gelombang(e.Group("/gelombang"))
	Pendaftar(e.Group("/pendaftar"))

	e.Logger.Fatal(e.Start(":1323"))
}

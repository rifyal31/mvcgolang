package routers

import (
	"mvc/controllers"

	"github.com/labstack/echo/v4"
)

func Gelombang(p *echo.Group) {
	p.GET("", controllers.Index)
	p.POST("/store", controllers.Store)
}

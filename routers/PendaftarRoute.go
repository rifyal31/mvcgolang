package routers

import (
	"mvc/controllers"

	"github.com/labstack/echo/v4"
)

func Pendaftar(p *echo.Group) {
	p.GET("", controllers.Index)
	p.POST("/store", controllers.StoreStatic)
	p.POST("/db/store", controllers.Store)
}

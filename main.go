package main

import (
	"mvc/config"
	"mvc/routers"
)

func main() {
	config.InitDB()
	routers.Web()
}

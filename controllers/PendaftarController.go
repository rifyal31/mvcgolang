package controllers

import (
	"mvc/config"
	"mvc/helpers"
	"mvc/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func Index(c echo.Context) error {
	model := []models.Pendaftar{}
	res := helpers.BaseResponse{}

	db := config.InitDB().Table("pendaftar").Find(&model)
	if db.Error != nil {
		res := helpers.BaseResponse{
			Code:    http.StatusInternalServerError,
			Message: helpers.MessageFailed,
			Data:    nil,
		}
		return c.JSON(http.StatusOK, res)
	}

	res = helpers.BaseResponse{
		Code:    http.StatusOK,
		Message: helpers.MessageSuccess,
		Data:    model,
	}
	return c.JSON(http.StatusOK, res)
}
func StoreStatic(c echo.Context) error {
	nama := c.FormValue("nisn")
	nisn, _ := strconv.Atoi(c.FormValue("nisn"))
	ibu_kandung := c.FormValue("ibu_kandung")
	status, _ := strconv.Atoi(c.FormValue("status"))
	arr := map[string]interface{}{
		"nama":        nama,
		"nisn":        nisn,
		"ibu_kandung": ibu_kandung,
		"status":      status,
	}
	return c.JSON(http.StatusOK, arr)
}

func Store(c echo.Context) error {
	return c.JSON(http.StatusOK, "OKE")
}
